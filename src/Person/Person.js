import './Person.css';

function Person(props) {
    return (
        <div className="Person">
             <p onClick={props.click}>My name is {props.name} and I am {props.age} years old. </p>
             
             {/* use chilren property to render the text that is given between the Person element tag in parent component */}
             <p>{ props.children }</p>
             <input type="text" onChange={props.changed} value={props.name}/>
        </div>
    )
}

export default Person