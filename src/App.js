import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person';

function App() {
  const [personState, setPersonState] = useState([
    { id: 1, name: 'Max', age: 28 },
    { id: 2, name: 'John', age: 29 },
    { id: 3, name: 'Marie', age: 30 }
  ])

  const [showPerson, setShowPersonState] = useState(true);

  // const switchNameHandler = (newName) => {
  //   setPersonState([
  //     { id: 1, name: newName, age: 28 },
  //     { id: 2, name: 'Lorem Ipsum', age: 29 },
  //     { id: 3, name: 'Marie', age: 30 }
  //   ])
  // }

  const nameChangeHandler = (event, id) => {
    const personIndex = personState.findIndex((person) => person.id == id);
    const person = { ...personState[personIndex] };
    person.name = event.target.value;

    const persons = [...personState];
    persons[personIndex] = person;

    setPersonState(persons)
  }

  const deletePersonHandler = (personIndex) => {
    const persons = [...personState];
    persons.splice(personIndex, 1);
    setPersonState(persons);
  }

  const togglePersonsHandler = () => {
    setShowPersonState(!showPerson)
  }

  // inline styling
  const style = {
    backgroundColor: 'white',
    font: 'inherit',
    border: '1px solid blue',
    padding: '8px',
    cursor: 'pointer'
  }

  let persons = null;
  if (showPerson) {
    persons = (
      <div>
        {
          personState.map((person, index) => {
            return <Person
              click={() => deletePersonHandler(index)}
              name={person.name}
              age={person.age}
              key={person.id }
              changed={(event) => nameChangeHandler(event, person.id)}
            />
          })
        }

        {/* <Person
          name={personState[1].name}
          age={personState[1].age}>
          My Hobbies: Racing
        </Person>
        <Person
          name={personState[2].name}
          age={personState[2].age}
          click={() => switchNameHandler('John Carter')}
          change={(event) => nameChangeHandler(event)} /> */}
      </div>
    )
  }

  return (
    <div className="App">
      <button
        style={style}
        onClick={togglePersonsHandler}>
        Toggle Person List
      </button>

      {persons}

    </div>
  );

  // This is how above code is done without JSX
  // return React.createElement('div', { className: 'App' }, React.createElement('h1', null, 'Hello'))
}

export default App;
